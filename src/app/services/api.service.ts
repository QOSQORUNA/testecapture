import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { UserRequest } from '../models/request/user.request';
import { ListRegisterResponse } from '../models/response/list_register.response';
import { RegisterFormResponse } from '../models/response/register_form.response';
import { ActivitiesByLineResponse } from '../models/response/activities_by_line.response';
import { RegisterRequest } from '../models/request/register.request';
import { RegisterDetailRequest } from '../models/request/register-detail.request';



@Injectable({
  providedIn: 'root'
})
export class ApiService {
  private urlBase = 'http://190.156.254.66:50026/api/v1';
  token: string;
  constructor(private http: HttpClient) {
    this.leerToken();
  }
  // guardar y leer token en localStorage

  private guardarToken(token: string) {
    this.token = token;
    localStorage.setItem('token', token);
  }
  private leerToken() {
    if ( localStorage.getItem('token')) {
      this.token = localStorage.getItem('token');

    } else   {
      this.token = '';
    }
    return this.token;

  }
  isAuth(): boolean {
    console.log(this.token);
    return this.token.length > 2;
  }
  logout() {
    localStorage.removeItem('token');
  }
  // end
  
  loginToken(data: UserRequest) {
    return this.http.post(`${this.urlBase}/login`, data).pipe(
      map( res => {
        this.guardarToken(res['data']);
        return res;
      }
      ));
  }
  getRegisters(): Observable<ListRegisterResponse[]> {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${ this.token }`
    });
    return this.http.get<ListRegisterResponse[]>(`${this.urlBase}/registro_inspecciones`, { headers });

  }
  getVehiculoByPlaca(placa: string): Observable<RegisterFormResponse[]> {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${ this.token }`
    });
    return this.http.get<RegisterFormResponse[]>(`${this.urlBase}/vehiculos?placa=${placa}`, { headers }).pipe(
      map(res => {
        res = res['data'];
        return res;
      })
    );

  }
  getActivitiesByLine(line: number): Observable<ActivitiesByLineResponse[]> {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${ this.token }`
    });
    return this.http.get<ActivitiesByLineResponse[]>(`${this.urlBase}/apl?linea=${line}`, { headers }).pipe(
      map(res => {
        res = res['data'];
        return res;
      })
    );

  }
  saveRegister(data: RegisterRequest) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${ this.token }`
    });
    return this.http.post(`${this.urlBase}/registro_inspecciones`, data, { headers });

  }
  saveRegisterDetail(data: RegisterDetailRequest) {
    const headers = new HttpHeaders({
      Authorization: `Bearer ${ this.token }`
    });
    return this.http.post(`${this.urlBase}/detalle_inspecciones`, data, { headers });

  }
}
