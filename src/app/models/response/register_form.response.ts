export class RegisterFormResponse {
    id: number;
    placa: string;
    carroceria: string;
    componente: string;
    marca_chasis: string;
    linea_chasis: string;
    tipo_combustible: string;
    numero_chasis: string;
    anio_modelo: number;
    numero_motor: string;
}