export class ActivitiesByLineResponse {
    id: number;
    id_linea: number;
    id_actividad: number;
    id_categoria: number;
    nombre_categoria: string;
    nombre_actividad: string;
}