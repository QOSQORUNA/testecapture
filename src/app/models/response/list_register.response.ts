export class ListRegisterResponse {
    id_vehiculo: number;
    fecha_programada: Date;
    kilometraje: number;
    id_usuario: number;
    fecha_ejecucion_inspeccion: Date;
    observaciones: string;    
}