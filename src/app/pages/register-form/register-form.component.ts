import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { RegisterFormResponse } from '../../models/response/register_form.response';
import { ApiService } from '../../services/api.service';
import { ActivitiesByLineResponse } from '../../models/response/activities_by_line.response';
import { RegisterRequest } from 'src/app/models/request/register.request';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {

  currentDate = new Date();
  
  registerGeneralObs: FormGroup
  registerGeneral: FormGroup;
  checkform: FormGroup;
  registerForm: FormGroup;
  validarPlaca: FormGroup;
  vehiculo: RegisterFormResponse;
  categorias: ActivitiesByLineResponse;

 
  constructor(private fb: FormBuilder,
              private api: ApiService) {
                this.checkform = this.fb.group({
                  isArray: this.fb.array([], [Validators.required])
                })
               }

  ngOnInit(): void {
    this.createFormPlaca();
    this.getActivitiesByLine();
    this.createFormRegister();
    this.createFormRegisterObs();
   
  }

  private createFormRegister(){ 
    this.registerGeneralObs = this.fb.group({
      observaciones: new FormControl(''),
    })
    
  }
  private createFormRegisterObs(){ 
    this.registerGeneral = this.fb.group({
      kilometraje: new FormControl(''),
    })
    
  }

  private createFormPlaca(){ 
    this.validarPlaca = this.fb.group({
      placa: new FormControl(''),
    })
    
  }
  private createForm() {
    this.registerForm = new FormGroup({
      id: new FormControl(this.vehiculo ? this.vehiculo.id: '', Validators.required),
      placa: new FormControl(this.vehiculo ? this.vehiculo.placa: '', Validators.required),
      carroceria: new FormControl(this.vehiculo ? this.vehiculo.carroceria: '', Validators.required),
      componente: new FormControl(this.vehiculo ? this.vehiculo.componente: '', Validators.required),
      marca_chasis: new FormControl(this.vehiculo ? this.vehiculo.marca_chasis: '', Validators.required),
      linea_chasis: new FormControl(this.vehiculo ? this.vehiculo.linea_chasis: '', Validators.required),
      tipo_combustible: new FormControl(this.vehiculo ? this.vehiculo.tipo_combustible: '', Validators.required),
      numero_chasis: new FormControl(this.vehiculo ? this.vehiculo.numero_chasis: '', Validators.required),
      anio_modelo: new FormControl(this.vehiculo ? this.vehiculo.anio_modelo: '', Validators.required),
      numero_motor: new FormControl(this.vehiculo ? this.vehiculo.numero_motor: '', Validators.required),
        });
  }
  onValidatePlaca(){
    this.api.getVehiculoByPlaca(this.validarPlaca.value.placa).subscribe(async (data: any) => {
      this.vehiculo = data;
      await this.createForm();
    });
  }

  saveRegister() {

    let rq = new RegisterRequest();

    rq.id_vehiculo = this.vehiculo.id;
    rq.kilometraje = parseInt(this.registerGeneral.value.kilometraje);
    rq.observaciones = this.registerGeneralObs.value.observaciones;

    console.log( rq);
    
    this.api.saveRegister(rq).subscribe(res => {
      console.log(res);
    });
  }

  getActivitiesByLine(){
    this.api.getActivitiesByLine(2).subscribe((data: any) => {
      this.categorias = data;
    });
  }
  onChange(e){
  const isArray: FormArray = this.checkform.get('isArray') as FormArray;
  if (e.target.checked) {
    isArray.push(new FormControl(e.target.value));
  } else {
    let i: number= 0;
    isArray.controls.forEach((item: FormControl) => {
      if(item.value == e.target.value) {
        isArray.removeAt(i);
        return;  
      }
      i++;
    });
  }
  }
  onSubmit() {
    console.log(this.checkform.value);
  }
}
