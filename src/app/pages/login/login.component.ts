import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UserRequest } from '../../models/request/user.request';
import { ApiService } from '../../services/api.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  
  user: UserRequest;
  constructor(private api: ApiService,
              private router: Router) { }

  ngOnInit(): void {
    this.createForm();
    }
  
  private createForm() {
    this.loginForm = new FormGroup({
      user: new FormControl(this.user ? this.user.user: '', Validators.required),
      password: new FormControl(this.user ? this.user.password: '', Validators.required)
    })
  }
  onSave(){
    console.log(this.loginForm);
    this.api.loginToken(this.loginForm.value).subscribe(res=> {
      console.log(res);
      this.router.navigateByUrl("/home");
    });
  }

}
