import { Component, OnInit } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { ListRegisterResponse } from '../../models/response/list_register.response';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  resultado: Array<ListRegisterResponse>;

  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.getRegisters();
  }
  getRegisters() {
    this.api.getRegisters().subscribe(data => {
      this.resultado = data['data'];
    },
    err => {
      console.log(err);
    }
    );
    console.log(this.resultado);
  }

}
